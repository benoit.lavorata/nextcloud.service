#!/bin/bash
echo ""
echo "Nextcloud"
echo "Maintainer: Benoit Lavorata"
echo "License: MIT"
echo "==="
echo ""

FILE=".env"
if [ -f $FILE ]; then
    echo ""
    echo "File $FILE exists, will now start: ..."
    export $(cat .env | xargs)

    echo ""
    echo "Create networks ..."
    docker network create $NEXTCLOUD_CONTAINER_NETWORK
    docker network create $DB_CONTAINER_NETWORK

    echo ""
    echo "Create volumes ..."
    docker volume create --name $DB_CONTAINER_VOLUME_DATA
    docker volume create --name $NEXTCLOUD_CONTAINER_VOLUME_DATA
    docker volume create --name $NEXTCLOUD_CONTAINER_VOLUME_CONFIG
    docker volume create --name $NEXTCLOUD_CONTAINER_VOLUME_ADDONS
    docker volume create --name $NEXTCLOUD_CONTAINER_VOLUME_THEMES
    mkdir -p $NEXTCLOUD_CONTAINER_HOST_BIND_DATA
    

    echo ""
    echo "Boot ..."
    docker-compose up -d --remove-orphans $1

    #docker exec -u www-data $NEXTCLOUD_CONTAINER_NAME php occ --no-warnings config:system:set trusted_domains 0 --value="$NEXTCLOUD_CONTAINER_VHOST"
else
    echo "File $FILE does not exist: deploying for first time"
    cp .env.template .env
    
    echo -e "Make sure to modify .env according to your needs, then use ./up.sh again."
fi

